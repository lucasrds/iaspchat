create database if not exists chat_iasp;

use chat_iasp;

create table if not exists users(
	id integer NOT NULL AUTO_INCREMENT,
    username varchar(255),
    password varchar(255),
    fullname varchar(255),
    age int,
    sex boolean,
    PRIMARY KEY(id)
)
