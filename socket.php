<?php  
require 'vendor/autoload.php';  
use Ratchet\MessageComponentInterface;  
use Ratchet\ConnectionInterface;

require 'chat.php';

// Run the server application through the WebSocket protocol on port 8080
$app = new Ratchet\App("127.0.0.1", 8080, '0.0.0.0', $loop);
$app->route('/chat', new Chat, array('*'));

$app->run();