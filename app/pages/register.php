<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>IASP php</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="app/css/login.css" />
    <script src="app/js/register.js"></script>
</head>

<body>
    <div class="login-page">
        <div class="form">
            <form class="register-form" method="POST" action="/register" onsubmit="return doRegister(event);">
                <input type="text" class="w-100" name="fullname" placeholder="Nome" />
                <input type="number" min="18" max="99" class="w-100" name="age" placeholder="Idade" />
                <div>
                    <label> Sexo: </label>
                    <label><input type="radio" value="true" name="sex" placeholder="Sexo" checked/> Masculino </label>
                    <label><input type="radio" value="false" name="sex" placeholder="Sexo" /> Feminino </label>
                </div>
                <input type="text" class="w-100" name="username" placeholder="Usuário" />
                <input type="password" class="w-100" name="password" placeholder="Senha" />
                <button> Registrar </button>
            </form>
        </div>
    </div>
</body>

</html>