
(function (document, window) {

    window.addEventListener('load', () => {

        let websocket;
        let loggedUser;

        const getCookie = (name) => {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        }

        const liElement = (innerHTML) => {
            let li = document.createElement("li");
            li.className = "clearfix";
            li.innerHTML = innerHTML;
            return li;
        }

        const convertDate = (date) => {
            return `${date.getDay()}/${date.getMonth() + 1}/${date.getFullYear()} - ${date.getHours()}:${date.getMinutes()}`;
        }

        const createOtherMessageElement = (name, message) => {
            return liElement(`<div class="message-data align-left">
                <i class="fa fa-circle him"></i> <span class="message-data-name"> ${name} </span> 
                <span class="message-data-time"> ${convertDate(new Date())} </span> &nbsp; &nbsp;
            </div> <div class="message other-message float-left"> ${message} </div>`);
        };

        const createMyMessageElement = (name, message) => {
            return liElement(`<div class="message-data align-right">
                <span class="message-data-time"> ${convertDate(new Date())} </span> &nbsp; &nbsp;
                <span class="message-data-name"> ${name} </span> <i class="fa fa-circle me"></i>
            </div> <div class="message my-message float-right"> ${message} </div>`);
        };

        const addMessageElement = (element) => {
            messageListElement.appendChild(element);
            updateMessageQuantity();
        }

        const sendMessage = (contentElementId) => {
            let messageContent = document.getElementById(contentElementId).value;
            let fullMessage = JSON.stringify({
                user: loggedUser,
                content: messageContent
            });            
            websocket.send(fullMessage);
            addMessageElement(createMyMessageElement(loggedUser, messageContent));
            document.getElementById(contentElementId).value = '';

            chatHistoryElement.scrollTop = chatHistoryElement.scrollHeight;
        }

        const updateMessageQuantity = () => {
            messageQuantity++;
            messageQuantityElement.innerHTML = messageQuantity;
        }

        const setupWebsocket = () => {
            websocket = new WebSocket("ws://127.0.0.1:8080/chat");
            // websocket.onopen = () => websocket.send("Message sent");
            websocket.onclose = () => console.log("Connection is closed.");
            websocket.onmessage = (event) => {
                let receivedMessage = JSON.parse(event.data);
                addMessageElement(createOtherMessageElement(receivedMessage.user, receivedMessage.msg));
                
                chatHistoryElement.scrollTop = chatHistoryElement.scrollHeight;
            };
            window.onbeforeunload = (event) => websocket.close();
        }

        const messageListElement = document.getElementById('messages-list');

        const messageQuantityElement = document.getElementById('message-quantity');

        const chatHistoryElement = document.getElementById('chat-history');

        let messageQuantity = 0;

        function init() {
            loggedUser = getCookie("chatUsername");
            setupWebsocket();
        }
        init();

        document.sendMessage = sendMessage;

    });

})(document, window);