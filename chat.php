<?php  
use Ratchet\MessageComponentInterface;  
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {  
    public $clients;
    private $logs;
    private $connectedUsers;
    private $connectedUsersNames;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->logs = [];
        $this->connectedUsers = [];
        $this->connectedUsersNames = [];
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
        $this->connectedUsers [$conn->resourceId] = $conn;
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        
        $json = json_decode($msg);
        // echo var_dump($json);
        $this->sendMessage($from, array(
            "user" => $json->user,
            "msg" => $json->content,
            "timestamp" => time()
        ));
    }

    public function onClose(ConnectionInterface $conn) {
        // Detatch everything from everywhere
        $this->clients->detach($conn);
        unset($this->connectedUsersNames[$conn->resourceId]);
        unset($this->connectedUsers[$conn->resourceId]);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $conn->close();
    }

    private function sendMessage($conn, $message) {
        foreach ($this->connectedUsers as $user) {
            if($conn != $user){
                $user->send(json_encode($message));
            }
        }
    }
}