<?php

error_log($_SERVER["REQUEST_URI"], 4);

include_once("database.php");

if ($_SERVER["REQUEST_URI"] == "/" || $_SERVER["REQUEST_URI"] == "/login") {
    include_once("app/pages/login.php");
    include_once("app/dologin.php");
} else if ($_SERVER["REQUEST_URI"] == "/chat") {
    include_once("app/pages/chat.php");
} else if ($_SERVER["REQUEST_URI"] == "/register") {
    include_once("app/pages/register.php");
    include_once("app/doregister.php");
} else { 
    return false;
}

?>