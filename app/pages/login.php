<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>IASP php</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="app/css/login.css" />
    <script src="app/js/login.js"></script>
</head>

<body>
    <div class="login-page" >
        <div class="form">
            <form class="login-form" method="POST" action="/login">
                <input type="text" class="w-100" name="username" placeholder="Usuário" />
                <input type="password" class="w-100" name="password" placeholder="Senha" />
                <button>Login</button>
                <p class="message">
                    Não é cadastrado?
                    <a href="/register"> Crie uma conta </a>
                </p>
            </form>
        </div>
    </div>
</body>

</html>