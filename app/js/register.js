
(function (document, window) {

    const allValidatedTypes = ["text", "number", "password"];
    const inputsToBeValidated = ([i, input]) => allValidatedTypes.includes(input.type);
    const inputIsFilled = ([i, input]) => input.value.length;

    window.addEventListener('load', () => {
        const doRegister = (e) => {
            let inputs = e.target.getElementsByTagName("input");
            let submit = Object.entries(inputs)
                .filter(inputsToBeValidated)
                .every(inputIsFilled);
            if (!submit) alert(`
                Dados inválidos! 
                por favor preencha todos os campos corretamente
            `);
            return submit;
        }
        window.doRegister = doRegister;
    });

})(document, window);