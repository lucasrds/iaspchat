<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>IASP php</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<link rel="stylesheet" type="text/css" media="screen" href="app/css/chat.css"/>		
	<link rel="stylesheet" type="text/css" media="screen" href="vendor/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="app/css/css/fontawesome-all.css"/>
	<script src="app/js/websocket.js"> </script>
</head>

<body>
	<div class="panel panel-default w-100 h-100 inter-box">

	<nav class="navbar navbar-expand-lg navbar-dark">
		<a class="navbar-brand" href="#"> <img src="app/img/logo.png" width="50"/> UNASP - Canal de comunicação</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav mr-auto"></ul>
			<span class="navbar-text border-hr-left mr-2 pr-5">
				<div class="round-letter"> <?php echo strtoupper($_COOKIE["chatUsername"][0]) ?></div>
				<?php echo $_COOKIE["chatUsername"] ?>
			</span>
			<a class="nav-link" href="../"> 
				<i class="fas fa-sign-out-alt"></i>
				Sair 
			</a>
		</div>
	</nav>
		

	<div class="container-fluid fill">
		<div class="row fill">
			<div class="chat fill">
				<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center p-2 border-bottom">
					<h1 class="h2">CHAT</h1>
					<div class="btn-toolbar mb-2 mb-md-0">
						<b id="message-quantity"></b>
					</div>					 
				</div> 

				<div class="chat-history clearfix" id="chat-history">
					<ul id="messages-list"></ul> 
					<div class="chat-message clearfix">
					</div>
				</div>

				<div class="panel-footer fill">
					<div class="input-group p-3">
						<input name="message-to-send" id="message-to-send" type="text" class="form-control input-sm chat_input transparent" placeholder="Digite aqui ..."/>
						<span class="input-group-btn">
							<button class="btn btn-primary btn-sm" id="btn-chat" onclick="sendMessage('message-to-send')">
								<i class="fas fa-arrow-right"></i>
								Enviar Mensagem
							</button>
						</span>
					</div>
				</div>
			</div>
			<!--<nav class="col-md-2 d-none d-md-block sidebar">
				<div class="sidebar-sticky">				
					<h6 class="sidebar-heading d-flex justify-content-between align-items-center mt-4 mb-1">
						<span>Canais</span>
						<a class="d-flex align-items-center text-muted" href="#">
							<span data-feather="plus-circle"></span>
						</a>
					</h6>
					<ul class="nav flex-column mb-2">
						<li class="nav-item">
							<a class="nav-link active" href="#">                
								<span class="ligth-purple">@</span>Geral
							</a>
						</li>
					</ul>
					<div class="nav flex-column bottom-info">	       
						<span class="text-nowrap logged-info">IASP Chat (Logado como : <?php //echo $_COOKIE["chatUsername"] ?>)</span> 
					</div>
				</div>
			</nav>-->
		</div>
	</div>
	<script type="text/javascript">
		var input = document.getElementById("message-to-send");
		input.addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				document.getElementById("btn-chat").click();
			}
		});
	</script>
	</div>
</body>
</html>